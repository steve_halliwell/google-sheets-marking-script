//take a range and return all its stats
function Stats(range) 
{
  var height = range.length;
  //var width = range[0].length;
  
  var data = [];
  //extract data
  for (var i = 0; i < height; ++i)
  {
    var val = range[i];
    
    if(val != null && val != "" && typeof(val) == "number")
    {
      data.push(val);
    }
  }
  
  data.sort();
  data.reverse();
    
  return 1;
}

function GetData()
{
  return [1,2,3,4];
}

function TypeOf(cell)
{
  return Number(cell[0]) != NaN;
}

//takes number and returns as percentage
// takes hd+,hd, hd-,d+, d,  d-, c+, c,  c-, p+, p,  p-, e+, e,  e-, f and returns them as
//       99%,92%,87%,83%,80%,77%,73%,70%,68%,63%,58%,52%,40%,30%,20%,10%,
//returns percentage as are
function asMark(data)
{
  if(typeof(data) == "string")
  {
    switch(data)
    {
      case "hd+":
        return 0.98;
      case "hd":
        return 0.92;
      case "hd-":
        return 0.87;
      case "d+":
        return 0.83;
      case "d":
        return 0.80;
      case "d-":
        return 0.77;
      case "c+":
        return 0.73;
      case "c":
        return 0.70;
      case "c-":
        return 0.68;
      case "p+":
        return 0.63;
      case "p":
        return 0.58;
      case "p-":
        return 0.53;
      case "e+":
        return 0.4;
      case "e":
        return 0.3;
      case "e-":
        return 0.2;
      case "f":
        return 0.1;
        
      default:
        return null;
    }
  }
  else if(typeof(data) == "number")
  {
    if(data <= 1)
    {
      return data;
    }
    else if(data <= 100)
    {
      return data / 100;
    }
  }
  
  
  return null;
}

//treats all vales as a mark, returns total mark
function asMarkRangeSum(range)
{
  var height = range.length;
  var width = range[0].length;
  
  var retval = 0;
  
  for(var i = 0; i < height; ++i)
  {
    for(var j = 0; j < width; ++j)
    {
      retval += asMark(range[i][j]);
    }
  }
  
  return retval;
}

function asMarkRangeAv(range)
{
  var sum = asMarkRangeSum(range);
  
  return (sum / range.length) / range[0].length;
}

//treats all vales as a mark, returns total mark
function asMarkRangeSumWeighted(gradeRange, weightRange)
{
  var height = gradeRange.length;
  var width = gradeRange[0].length;
  
  var retval = 0;
  
  for(var i = 0; i < height; ++i)
  {
    for(var j = 0; j < width; ++j)
    {
      retval += asMark(gradeRange[i][j]) * weightRange[i][j];
    }
  }
  
  return retval;
}


//returns an array of numbers
function GenerateSeries(start, end, step)
{
  var retval = new Array();
  
  for(;start < end; start += step)
  {
    retval.push(start);
  }
  
  return retval;
}



//requires the col letter where the key of the key val pairs are, returns the select that 
// expects comma seperated terms
function GenComQGen(keyColLetter, valColLetter, commaData)
{
  var splitRes = commaData.split(",");
  var res = "SELECT " + valColLetter + " WHERE ";
  
  for(var i = 0; i < splitRes.length; ++i)
  {
    res += keyColLetter + "='" + splitRes[i].trim() + "' OR ";
  }
  
  //we don't need the last OR so
  return res.substring(0,res.length-3);
}

//concatenate with new lines between data
function ConcatNL(data)
{
  var res = "";
  
  if(typeof(data) != "string")
  {
    for(var i = 0; i < data.length; ++i)
    {
      res += data[i] + "\n";
    }
  }
  else
  {
    res = data;
  }  
  return res;
}

//return the series for frequencys that equate to f,p,c,d,hd
function GenerateGradeSeries(scale)
{
  if(scale)
    return [0.495*scale, 0.645*scale, 0.745*scale, 0.845*scale];
  else
    return [0.495, 0.645, 0.745, 0.845];
    
}